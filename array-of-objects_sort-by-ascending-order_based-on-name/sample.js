let a = [
    {
        name: "Hello",
        id: 1,
    },
    {
        name: "World",
        id: 4,
    },
    {
        name: "Zackaria",
        id: 2,
    },
    {
        name: "Penguin",
        id: 3,
    },
];

a.sort(function(a, b) {
    const A = a.name.toUpperCase();
    const B = b.name.toUpperCase();
    let comparison = 0;
    if (A > B) {
        comparison = 1;
    } else if (B > A) {
        comparison = -1;
    }
    return comparison;
});

console.log(a);