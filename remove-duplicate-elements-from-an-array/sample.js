

var fruits = ["apple", "mango", "apple", "orange", "mango", "mango"];
  
function removeDuplicates(fruits) {
    return fruits.filter((fruit, 
        index) => fruits.indexOf(fruit) === index);
}

console.log(removeDuplicates(fruits));